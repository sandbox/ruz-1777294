<?php
/**
 * @file
 * The theme will output the subheadings found in the array.
 */
?>
<ul class="topic-block">
    <?php foreach($subheadings as $value): ?>
      <li><?php print l($value['topic'], '', array('fragment' => $value['anchor'], 'external' => TRUE)); ?></li>
    <?php endforeach; ?>
</ul>
