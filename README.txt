Overview
Linked subheadings is perfect for pages with lots of content.
The module processes the body content of a node looking for headings.
It inserts anchors in front of the headings before the content is displayed.
A separate block will display all the subheadings which are linked to the 
content.

Features
Outputs block with subheadings.
Add anchors to the body of the node before output.
Can modify the depth of subheadings & the type of node to process.

Installation
- Install the module.
- Configure the module under Admin > Configuration > Content Authoring >
Linked subheadings (admin/config/content/subheading_links).
- Move the "Linked subheadings" block to display region.
 Documentation for blocks can be found here:
 http://drupal.org/documentation/modules/block
